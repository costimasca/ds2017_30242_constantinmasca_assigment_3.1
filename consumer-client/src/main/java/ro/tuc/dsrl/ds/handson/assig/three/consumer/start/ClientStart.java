package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.DVD;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.Message;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientStart {

	private ClientStart() {
	}


	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost", 8888);

		MailService mailService = new MailService("rentboardgames1234@gmail.com", "12345678!");

		while (true) {
			try {
				DVD d = (DVD)queue.readMessage();

					String mail = "New movie in store!\n" +
							d.getTitle() + ", " +
							d.getYear() + " for just " +
							d.getPrice() + "$\nOrder it now!\n";
					System.out.println("Sending mail:\n" + mail);

					mailService.sendMail("costimasca@gmail.com","New DVD added",mail);
				} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
