package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.DVD;
import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;

import java.io.IOException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer2).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);

		try {
			queue.writeMessage(new DVD("Eyes wide shut",1998,24));
			queue.writeMessage(new DVD("Duel",2000,37));
			queue.writeMessage(new DVD("Pee Wee's Big Adventure",1987,24));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
