package ro.tuc.dsrl.ds.handson.assig.three.consumer2.start;

import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.DVD;
import ro.tuc.dsrl.ds.handson.assig.three.consumer2.connection.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientWriterStart {

	private ClientWriterStart() {
	}

	private static void writeFile(DVD d) {
		try {
			PrintWriter writer = new PrintWriter("/home/constantin/Documents/sd/Movies" + d.getTitle(), "UTF-8");
			writer.println(d.getTitle());
			writer.println(d.getYear());
			writer.println(d.getPrice());
			writer.close();

			System.out.println("File written");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost", 8888);

		while (true) {
			try {
				DVD d = (DVD)queue.readMessage();
				writeFile(d);

				} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
