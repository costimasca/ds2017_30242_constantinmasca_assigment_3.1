package ro.tuc.dsrl.ds.handson.assig.three.queue.queue;

import com.sun.org.apache.xerces.internal.xs.datatypes.ObjectList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-server
 * @Since: Sep 1, 2015
 * @Description:
 * 	Wrapper for a queue, which provides only 2 methods, put() and get()
 * 	to insert and retrieve elements from the queue (functions as push
 * 	and pop in a FIFO manner).
 *
 * 	Underlying queue is a BlockingQueue; this type of queue will block
 * 	and wait for elements on retrieve, if there are currently no elements
 * 	in the queue.
 */
public class Queue {
    private static Queue queueInstance;
    private List<LinkedBlockingDeque<Object>> queues;

    private Queue() {
        queues = new ArrayList<>();
    }

    public void newClient() {
        queues.add(new LinkedBlockingDeque<Object>());
    }

    public static Queue getInstance() {
        if (queueInstance==null) queueInstance=new Queue();
        return queueInstance;
    }

    public void put(Object message) throws InterruptedException {
        for(int i = 0; i < queues.size();i++){
            queues.get(i).put(message);
        }
    }

    public Object get(int id) throws InterruptedException {
        return queues.get(id).take();
    }
}
